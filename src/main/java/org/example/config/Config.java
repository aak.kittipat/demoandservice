package org.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("file:${demo.config.path}")
public class Config {

    @Value("${service.testReturnList.url}")
    private String testReturnList;

    @Value("${service.testReturnString.url}")
    private String testReturnString;

    @Value("${service.getobject.url}")
    private String getobject;

    @Value("${service.putobject.url}")
    private String putobject;

    public String getTestReturnList() {
        return testReturnList;
    }

    public void setTestReturnList(String testReturnList) {
        this.testReturnList = testReturnList;
    }

    public String getTestReturnString() {
        return testReturnString;
    }

    public void setTestReturnString(String testReturnString) {
        this.testReturnString = testReturnString;
    }

    public String getGetobject() {
        return getobject;
    }

    public void setGetobject(String getobject) {
        this.getobject = getobject;
    }

    public String getPutobject() {
        return putobject;
    }

    public void setPutobject(String putobject) {
        this.putobject = putobject;
    }
}
