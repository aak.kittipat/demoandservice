package org.example.service;

import org.example.config.Config;
import org.example.domain.Domain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class WebClientService {

    @Autowired
    Config config;

    public Mono<String> testString() {
        return WebClient.create(config.getTestReturnString()).get().retrieve().bodyToMono(String.class);
    }

    // Flux<String> == Mono<List<String>>
    public Flux<String> testReturnList() {
        return WebClient.create(config.getTestReturnList()).get().retrieve().bodyToFlux(String.class);
    }

    public Mono<List<String>> test() {
        return WebClient.create(config.getTestReturnList()).get().retrieve().bodyToMono(new ParameterizedTypeReference<List<String>>() {
        });
    }


    public Mono<Domain> getObject(String key) {
        return WebClient.create(config.getGetobject() + "/" + key).get().retrieve().bodyToMono(Domain.class);
    }

    public Mono<String> putObject(String key, Domain domain) {
        return WebClient.create(config.getPutobject() + "/" + key).put().body(Mono.just(domain),Domain.class).retrieve().bodyToMono(String.class);
    }

    public void test(String key,Domain domain){
        WebClient.create(config.getPutobject() + "/" + key).put().body(Mono.just(domain),Domain.class).retrieve().bodyToMono(String.class);
        System.out.println("hello");
    }

}
