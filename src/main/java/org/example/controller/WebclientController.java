package org.example.controller;

import org.example.domain.Domain;
import org.example.service.WebClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/webclient")
public class WebclientController {

    @Autowired
    WebClientService service;

    @GetMapping("/TestString")
    public Mono<String> testString() {
        return service.testString();
    }

    @GetMapping("/TestReturnList")
    public Flux<String> testReturnList() {
        return service.testReturnList();
    }

    @GetMapping("/GetObject/{key}")
    public Mono<Domain> getObject(@PathVariable("key") String key) {
        return service.getObject(key);
    }

    @PutMapping("/PutObject/{key}")
    public Mono<String> putObject(@PathVariable("key") String key,@RequestBody Domain domain) {
        return service.putObject(key, domain);
    }

    @GetMapping("/test")
    public Mono<List<String>> test() {
        return service.test();
    }
}
