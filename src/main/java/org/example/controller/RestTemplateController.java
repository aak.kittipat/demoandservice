package org.example.controller;

import org.example.domain.Domain;
import org.example.service.RestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController {

    @Autowired
    RestTemplateService restTemplateService;

    @GetMapping("/TestString")
    public String restTemplateTestString() {
        return restTemplateService.testString();
    }

    @GetMapping("/TestReturnList")
    public List restTemplateTestReturnList() {
            List<String> test = restTemplateService.testReturnList();
           return restTemplateService.testReturnList();
    }

    @GetMapping("/GetObject/{key}")
    public String restTemplateGetObject(@PathVariable("key") String key) {
        return restTemplateService.getObject(key);
    }

    @PutMapping("/PutObject/{key}")
    public String restTemplatePutObject(@PathVariable("key")  String key, @RequestBody Domain domain) {
        return restTemplateService.putObject(key, domain);
    }
}
