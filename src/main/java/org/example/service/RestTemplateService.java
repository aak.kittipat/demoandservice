package org.example.service;

import org.example.config.Config;
import org.example.domain.Domain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RestTemplateService {

    @Autowired
    private Config config;

    public RestTemplate getRestTemplate() {
        RestTemplateBuilder builder = new RestTemplateBuilder();
        return builder.build();
    }

    private HttpHeaders getHeaderKeep() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public String testString() {
        return getRestTemplate().exchange(config.getTestReturnString(), HttpMethod.GET, new HttpEntity<>(getHeaderKeep()),String.class).getBody();
    }

    public List<String> testReturnList() {
        return getRestTemplate().exchange(config.getTestReturnList(),HttpMethod.GET, new HttpEntity<>(getHeaderKeep()),new ParameterizedTypeReference<List<String>>() {
        }).getBody();
    }

    public String getObject(String key) {
        return getRestTemplate().exchange(config.getGetobject() + "/" + key, HttpMethod.GET, new HttpEntity<>(getHeaderKeep()),String.class).getBody();
    }

    public String putObject(String key,Domain domain) {
        return getRestTemplate().exchange(config.getPutobject() + "/" + key, HttpMethod.PUT, new HttpEntity<>(domain,getHeaderKeep()), String.class).getBody();
    }

}
